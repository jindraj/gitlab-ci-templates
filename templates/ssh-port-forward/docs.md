# SSH port forward
SSH port forward component enables your pipeline to use open LocalForward(s) [ssh_config(5)](https://man.openbsd.org/ssh_config#LocalForward)) for your needs.

## Usage
```yaml
include:
  - component: $CI_SERVER_HOST/jakub.jindra/gitlab-ci-templates/ssh-port-forward@$SSH_PORT_FORWARD_VERSION
    inputs:
      ssh_host: ubuntu
      ssh_user: jumphost.example.com
      ssh_private_key: $SSH_PRIVATE_KEY
      ssh_forwarding_endpoints: 15432:postgresql.example.com:5432 16379:redis.example.com:6379

example:
  extends:
    - .ssh_port_forward
  script:
    - psql -U user -h localhost -p 15432 -c 'SELECT 1'
    - redis-cli -p 16379 PING
```

## Inputs
### `ssh_host`

SSH server to connect to and open LocalForwards from.

example: `jumphost.example.com`

### `ssh_user`

Login username for `ssh_host`

example: `ubuntu`

### `ssh_private_key`

Private key for authentication configured with `ssh_user` on `ssh_host`

example: `/path/to/a/private/key/id_rsa`
example: 
```
-----BEGIN OPENSSH PRIVATE KEY-----
b3BlbnNzaC1rZXktdjEAAAAABG5vbmUAAAAEbm9uZQAAAAAAAAABAAAAMwAAAAtzc2gtZW
…
-----END OPENSSH PRIVATE KEY-----
```


### `ssh_port_forwarding_endpoints`

A whitespace separated list of LocalForwards to open. Each LocalForward is in format LOCAL_PORT:REMOTE_HOST:REMOTE_PORT.

example: `15432:postgres.example.com:5432 13306:mysql.example.com:3306 16379:redis.example.com:6379`
